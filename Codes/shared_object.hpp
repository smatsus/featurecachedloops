#ifndef _SHARED_OBJECT_HPP_
#define _SHARED_OBJECT_HPP_

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <cstring>

#include <sys/stat.h>
#include <kcdb.h>
#include <kchashdb.h>
#include <kcstashdb.h>

#include <atomic>
#include <mutex>
#include <chrono>

using namespace kyotocabinet;

class Shared_object{

public:
  std::atomic<bool> stop_flag;
  std::atomic<bool> exit_flag;
  std::atomic<size_t> reader_iter;
  std::atomic<size_t> trainer_iter;

  size_t num_data_; // number of data
  size_t num_feature_; // number of features
  vector<int> y; // label of data
  size_t num_class_;

  vector<int> ytest; // label of data
  vector<vector<ColIndexType> > Xtest_index; // label of data
  vector<vector<ValueType> > Xtest_value;
  bool have_test;
  
  string kch_path;
  string meta_path;
  StashDB memory_db;
  
  size_t memory_capacity;

  std::atomic<int> working_set_size;
  int working_set_max_size;
  std::mutex working_set_mutex;
  vector<int> working_set;
    
  Shared_object(options& p);
  ~Shared_object();

  void get_ln(string mfpath);
  void set_memory_db(int _working_set_max_size, int _memory_capacity);
  size_t memory_size();
  bool get_columns(vector<string> keys, map<string,string> *columns, int buf_size);
	bool get_column(int &rand_idx,
                  ColIndexType &key,
                  char* one_column,
                  size_t &nnz);
  bool remove_column(ColIndexType key, int rand_idx);
  void random_evict();
  bool add(ColIndexType key, const char* add_cval, int nnz);
  bool add_bulk(map<string,string> &columns, const size_t total_bytes);

};

Shared_object::Shared_object(options& p):
      kch_path(p.train_kch_path),
      meta_path(p.train_meta_path){
    stop_flag=false;
    exit_flag=false;
    reader_iter=0;
    trainer_iter=0;
    get_ln(meta_path);
    y.resize(num_data_);      
  }
Shared_object::~Shared_object(){}

void Shared_object::get_ln(string mfpath){
  ifstream f(mfpath.c_str());
  if(f.is_open()==false){
    cout << "cannot read meta file." << endl;
    exit(1);
  }
  f >> num_data_ >> num_feature_;
}
    
void Shared_object::set_memory_db(int _working_set_max_size, int _memory_capacity){

  std::lock_guard<std::mutex>lock(working_set_mutex);

  working_set_max_size=_working_set_max_size;
  memory_capacity=_memory_capacity;    
  working_set_size=0;
  working_set.resize(working_set_max_size);
  memory_db.tune_buckets(working_set_max_size);
  memory_db.open("", StashDB::OWRITER | StashDB::OCREATE);
}
  
size_t Shared_object::memory_size(){
  int64_t s=memory_db.size();
  return (size_t) s >> 20;
}

bool Shared_object::get_columns(vector<string> keys, map<string,string> *columns, int buf_size){
  std::lock_guard<std::mutex>lock(working_set_mutex);

  if(buf_size >=  working_set_size)
    return false;
  int stale_size=(working_set_size-1);
  int rand_idx = rand()%stale_size;
  for(size_t i=0; i<buf_size; ++i){
    keys[i] = reinterpret_cast<char *> (&working_set[(rand_idx+i)%stale_size]);        
  }
  memory_db.get_bulk(keys, columns);
    
  return true;
}
bool Shared_object::get_column(int &rand_idx,
                               ColIndexType &key,
                               char* one_column,
                               size_t &nnz){
  std::lock_guard<std::mutex>lock(working_set_mutex);
  if(1 >=  working_set_size)
    return false;
  rand_idx = rand()%(working_set_size-1); // BUGBUG ???
  key = working_set[rand_idx];
    
  int32_t one_size = memory_db.get( (char*) &key,
                                    sizeof(ColIndexType),
                                    one_column,
                                    num_data_*SIZE_OF_UNIT );
  if(one_size != -1){
    nnz = one_size / SIZE_OF_UNIT;
    return true;
  }
  return false;	// get error はありえる
}

bool Shared_object::remove_column(ColIndexType key, int rand_idx){
  std::lock_guard<std::mutex>lock(working_set_mutex);
  if(1 >=  working_set_size)
    return false;
  if(working_set[rand_idx]==key){
    if( memory_db.remove((char*)&key, sizeof(ColIndexType)) ){
      working_set[rand_idx]=working_set[(int)--working_set_size];
      return true;
    }
    else{
      // This will happen when multiple threads tried to remove the same entry
      return false;
      // cout<< " evict error. working_set[" << rand_idx << "] ==" <<working_set[rand_idx]<<  " == key " << key <<endl;
      // cout << "but key doesn't exit in memory_db. memory_db.size:" << memory_db.count() << "workingsetsize:" << working_set_size <<endl;
      // exit(1);
    }
  }
  return false;        
}
    
void Shared_object::random_evict(){
  int rand_idx=rand()%(working_set_size-1); 
  ColIndexType key=working_set[rand_idx];
  remove_column(key, rand_idx);
}
bool Shared_object::add(ColIndexType key, const char* add_cval, int nnz){	
  while(memory_size() >= memory_capacity || working_set_size >= working_set_max_size ){
    random_evict();
  }
  std::lock_guard<std::mutex>lock(working_set_mutex);
  if( working_set_size < working_set_max_size ){
    if( memory_db.add( (char*)&key,
                       sizeof(ColIndexType),
                       add_cval,
                       nnz * SIZE_OF_UNIT  )){
      working_set[working_set_size++] = key;
      return true;
    }
    else{
      cout << "add error (active < max)" << endl;
      exit(1);
    }
  }
  else
    return false;
}
bool Shared_object::add_bulk(map<string,string> &columns, const size_t total_bytes){
  while(memory_size()  >= memory_capacity + total_bytes || working_set_size >= working_set_max_size + columns.size()){
    random_evict();
  }

  std::lock_guard<std::mutex>lock(working_set_mutex);
  if( memory_db.set_bulk(columns) ){
    for(std::map<string,string>::iterator iter = columns.begin(); iter != columns.end(); ++iter){
      working_set[working_set_size++] = *reinterpret_cast<const ColIndexType *>(iter->first.data());
      // memcpy((void *)iter->first.data() ,
      //        (void *)working_set.data() + static_cast<int>(working_set_size++)*sizeof(ColIndexType), sizeof(ColIndexType));
    }
    // for(int g=0; g< working_set_size; ++g)
    //   cout << "working_set[" << g << "] = " << working_set[g]  << endl;  
    return true;
  }
  else{
    cout << "add error (active < max)" << endl;
    exit(1);
  }
    

  return true;

}
#endif
