#ifndef _MULTICLASS_WEIGHT_H_
#define _MULTICLASS_WEIGHT_H_

#include "weight.hpp"

class multiclass_weight {
public:
  size_t num_class_;
  map<ColIndexType, vector<ValueType> >contents;
//  vector<weight> w;
  
  multiclass_weight(size_t num_class):
      num_class_(num_class)
//      ,w(num_class)
    {}
  ~multiclass_weight(){}
        
  void set(ColIndexType j, const vector<ValueType> &w_j){
    for( auto w_jk : w_j ){
      if(fabs(w_jk) > 1e-15){
        auto it=contents.find(j);
        if(it==contents.end())
          contents.emplace(j, w_j);
        else
          it->second.assign(w_j.begin(), w_j.end());
        return;
      }
    }
    contents.erase(j);
    // for(size_t k=0; k<num_class_; ++k)
    //   w[k]._set(j,w_j[k]);
  }
  void get(ColIndexType j, vector<ValueType> &w_j){
    auto it=contents.find(j);
    if(it==contents.end())
      return w_j.assign(num_class_, 0.0);
    else
      return w_j.assign(it->second.begin(), it->second.end());
    
    // for(size_t k=0; k<num_class_; ++k)
    //    w_j[k] = w[k]._get(j);
  }
  double l1_norm(){
    double ret=0.0;
    for(auto it=contents.begin(); it!=contents.end();++it){
      for(auto wkj : it->second)
        ret += fabs(wkj);
    }
    return ret;
    // for(size_t k=0; k<num_class_; ++k)
    //   ret += w[k].l1_norm();
    // return ret;
  }
  size_t size(){
    size_t ret=0;
    for(auto it=contents.begin(); it!=contents.end();++it)
      for(auto wjk : it->second) 
        ret += (wjk < 1e-15)? 0:1;
    // for(size_t k=0; k<num_class_; ++k)
    //   ret += w[k].size();
    return ret;
  }

  void print(string &filename){
    ofstream ofs;
    ofs.open(filename);
    ofs << "solver_type L1R_LR" <<endl;
    ofs << "nr_class 2" <<endl;
    ofs << "label -1 1" <<endl;
    ofs << "nr_feature 119" <<endl;
    ofs << "bias -1" <<endl;
    ofs << "w" <<endl;
    for(ColIndexType j=1; j< 120; ++j){
      auto it=contents.find(j);
      if(it==contents.end())
        ofs << "0.0" <<endl;
      else
        ofs << 2*(it->second)[0] <<endl;
    }
    
    // for(auto it=contents.begin(); it!=contents.end();++it){
    //   ofs << static_cast<int>(it->first) << ":";
    //   for(auto wkj : it->second)
    //     ofs <<  wkj << " ";
    //   ofs << endl;
    // }
    ofs.close();
  }
};
#endif
