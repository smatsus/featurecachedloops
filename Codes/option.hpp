#ifndef _OPTIONS_H_
#define _OPTIONS_H_

#include <boost/program_options.hpp>

#include <iostream>
#include <fstream>

using namespace boost::program_options;
using namespace std;

class options {//options which will be specified at the command line
public:
  bool debug;
  
  std::string  train_path;  // Name of training file
  std::string  train_datasetname; 
  std::string  train_kch_path; 
  std::string  train_meta_path; 
  std::string  train_label_path; 
  std::string  model_file;
  std::string  output_file;
  
  size_t max_iter;
  double print_period;
  double param_C;
//  int param_memory_size;
  int param_memory_capacity;
    
  ~options(){}
  options(const options &p);
  
  options(int argc, char* argv[]){
    
    options_description required("Required arguments");
    required.add_options()
      ("help,h", "produce help message and exit")
      ("debug", "debug mode (read file and then train) ")            
      ("train_file,f",
       value<string>(&train_path)->default_value("../Data/a1a"),
       "Name of training file folder")
      ("max_iter,i",
       value<size_t>(&max_iter)->default_value(100),
       "option number of iterations")
      ("period,p",
       value<double>(&print_period)->default_value(10),
       "output results period")
      // ("memory_size,m",
      //  value<double>(&param_memory_size)->default_value(0.5),
      //  "memory_size ( as a ratio to #features)")
      ("memory_capacity,o",
       value<int>(&param_memory_capacity)->default_value(4000),
       "memory_capacity by MB")
      ("C,c",
       value<double>(&param_C)->default_value(1.0),
       "hyperparameter");

    variables_map vm;
    store(parse_command_line(argc, argv, required), vm);
    notify(vm);
    
    if(vm.count("help")){
      cout << required << endl;
      exit(1);
    }
        
    debug = vm.count("debug");
    
    train_datasetname = train_path.substr(train_path.rfind("/")+1,train_path.size()-1);
    train_kch_path = train_path + "/" + train_datasetname + ".kch";
    train_meta_path = train_path + "/" + train_datasetname + "_meta";
    train_label_path = train_path + "/" +  train_datasetname + "_label";
  }
};
#endif
