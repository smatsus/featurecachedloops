#ifndef _TRAINER_H_
#define _TRAINER_H_

#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cmath>
#include <cstring>

#include <sys/stat.h>
#include <kcdb.h>
#include <kchashdb.h>
#include <kcstashdb.h>

#include <atomic>
#include <mutex>
#include <chrono>

#include "shared_object.hpp"
#include "binaryclass_weight.hpp"


using namespace std;
  using std::chrono::duration_cast;
  using std::chrono::milliseconds;

#define DURATION(X) (static_cast<double>(duration_cast<milliseconds>(X).count()) / 1000)
class trainer{
public:
  typedef std::chrono::steady_clock steady_clock;
  
  Shared_object &shared;
  options &p;

  const size_t max_iter;
  int period;

  int l;
  int n;
  
  double Gmax_init;
  double Gmax_old;
  double Gmax_new;

  size_t num_access;
  size_t num_update;
  size_t num_evict;

  std::chrono::steady_clock::time_point start;
  std::chrono::steady_clock::time_point end;		

  bool _print;

  trainer(const trainer &r);
  trainer(Shared_object &s, options &_p);
  ~trainer();

  virtual double calc_obj() = 0;
  virtual void subroutine(char* one_column) = 0;
  virtual void show_statistics() = 0;

	void operator()();

};

trainer::trainer(const trainer &r):
    shared(r.shared),
    p(r.p),
    max_iter(r.max_iter),
    period(r.period),
    l(r.l),
    n(r.n),
    Gmax_old(r.Gmax_old),
    Gmax_new(r.Gmax_new),
    Gmax_init(r.Gmax_init),
    num_access(r.num_access),
    num_update(r.num_update),
    num_evict(r.num_evict)
{}
trainer::trainer(Shared_object &s, options &_p):
    shared(s),
    p(_p),
    max_iter(p.max_iter*s.num_feature_),
    period( (p.print_period > 0) ? s.num_feature_*p.print_period : -p.print_period),
    l(s.num_data_),
    n(s.num_feature_),
    Gmax_old(1e10),
    Gmax_new(0.0),
    Gmax_init(1.0),
    num_access(0),
    num_update(0),
    num_evict(0){}
trainer::~trainer(){}
void trainer::operator()(){
  if(p.debug)
    cout <<"debug mode"<<endl;
  start = steady_clock::now();
  cout <<"trainer clock started."<<endl;
  char *one_column = new char[l*SIZE_OF_UNIT];
  int maintainance_period = l;
  _print=false;
  for(;shared.exit_flag == 0; ++shared.trainer_iter){
    while(shared.stop_flag==true){
      sleep(0.001);
    }      
    if(shared.trainer_iter % maintainance_period == 0){
      if(shared.trainer_iter == maintainance_period)
        Gmax_init = Gmax_new;      
      Gmax_old = Gmax_new;      
      Gmax_new = 0.0;      
    }      
    if(shared.trainer_iter % period == 0){
      shared.stop_flag=true;
      show_statistics();
      shared.stop_flag=false;
    }
    if(max_iter <= shared.trainer_iter){
      break;
    }
    subroutine(one_column);            
  }

  delete [] one_column;        
  end = steady_clock::now();
  cout << "triainer time: " << DURATION(end-start) << " seconds" << endl;
  cout << "objective function: " << calc_obj() << " " << endl;
  shared.exit_flag=true;

}
#endif
