#ifndef _BINARYCLASS_WEIGHT_H_
#define _BINARYCLASS_WEIGHT_H_

#include "weight.hpp"

class binaryclass_weight : public weight {
public:

  binaryclass_weight(){}
  ~binaryclass_weight(){}

  void set(ColIndexType j, ValueType w_j){
    _set((WeightKeyType)j, w_j);
  }
  ValueType get(ColIndexType j){
    return _get((WeightKeyType)j);
  }

  ValueType operator [] (ColIndexType j){
    return _get((WeightKeyType)j);
  }

  void print(){
    for(auto it=contents.begin(); it!=contents.end();++it)
      cout << it-> first << ":" << it->second << endl;    
  }

};
#endif
