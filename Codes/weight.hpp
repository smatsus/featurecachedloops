#ifndef _WEIGHT_H_
#define _WEIGHT_H_

class weight {
public:

  map<WeightKeyType, ValueType> contents;
  // virtual void set(WeightKeyType key, ValueType value) =0;
  // virtual ValueType get(WeightKeyType key) =0;

  weight(){}
  ~weight(){}
        

  void _set(WeightKeyType key, ValueType value);
  ValueType _get(WeightKeyType key);
  size_t size();
  double l1_norm();

  
};



void weight::_set(WeightKeyType key, ValueType value){
  if(fabs(value)<1e-15){
    contents.erase(key);
    return;
  }
  auto it=contents.find(key);
  if(it==contents.end())
    contents.emplace(key,value);
  else
    it->second=value;
}
ValueType weight::_get(WeightKeyType key){
  auto it=contents.find(key);
  if(it==contents.end())
    return 0.0;
  else
    return it->second;
}
size_t weight:: size(){
  return contents.size();
}
double weight::l1_norm(){
  double ret=0.0;
  for(auto it=contents.begin(); it!=contents.end();++it)
    ret += fabs(it->second);
  return ret;
}     

#endif
