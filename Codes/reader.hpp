#ifndef _READER_H_
#define _READER_H_

#include <iostream>
#include <cstdlib>
#include <cmath>
#include <cstring>

#include <sys/stat.h>
#include <kcdb.h>
#include <kchashdb.h>
#include <kcstashdb.h>

#include <atomic>
#include <mutex>
#include <chrono>

#include "shared_object.hpp"

using namespace std;
using std::chrono::duration_cast;
using std::chrono::milliseconds;
typedef std::chrono::steady_clock steady_clock;

class Reader{
protected:
  
  Shared_object &shared;
  options &p;
  HashDB file_db;    
public:
  Reader(const Reader &r);
  Reader(Shared_object &_s, options &_p);
  virtual void operator () () = 0;
};

class reader: public Reader{
public:
  reader(const reader &r);
  reader(Shared_object &_s, options &_p);
  void open();
  
	void operator() ();
  void add (int key, ColIndexType ukey, char* one_column);
  
};

Reader::Reader(const Reader &r):
    shared(r.shared),
    p(r.p),
    file_db(){}
Reader::Reader(Shared_object &_s, options &_p):
    shared(_s),
    p(_p){}

reader::reader(const reader &r):Reader(r){}
reader::reader(Shared_object &_s, options &_p):Reader(_s,_p){}

void reader::open(){
  struct stat st;
  if(stat(p.train_kch_path.c_str(),&st)==1){
    cout << p.train_kch_path.c_str() << " does not exists" << endl;
    exit(1);
  }
  else
    cout << "read " << p.train_kch_path.c_str() << endl;
  file_db.open(p.train_kch_path.c_str(), HashDB::OREADER);
  cout << "kch file opened" << endl;
  
}
  

void reader::operator() (){
  reader::open();
  steady_clock::time_point start;
  steady_clock::time_point end;	 
  start = steady_clock::now();
  if(p.debug)
    cout << "reader (debug mode) clock started" << endl;
  else
    cout << "reader clock started" << endl;

  const int num_feat = shared.num_feature_;
  char* one_column= new char [shared.num_data_*SIZE_OF_UNIT];
    

  for(int key=0 ; shared.exit_flag==false;++shared.reader_iter){
    while(shared.stop_flag==true){ sleep(0.001); }
    key = (key) % num_feat + 1;
    // key begins with 1.
    ColIndexType ukey = key;	
    if(shared.memory_db.check((char*)&ukey, sizeof(ColIndexType))==-1){
      add(key, ukey, one_column);
    }else if(p.debug){
      cout << "size " << shared.working_set_size << endl; 
      break;
    }
      
  }
  end = std::chrono::steady_clock::now();
  cout << "reader " << (static_cast<double>(std::chrono::duration_cast<milliseconds>(end-start).count()) / 1000)
       << " seconds" << endl;
  delete [] one_column;
}

  
void reader::add (int key, ColIndexType ukey, char* one_column) {
  int32_t one_size = file_db.get( (char*) &key, sizeof(int),
                                  one_column, shared.num_data_*SIZE_OF_UNIT );
  if(one_size != -1){
    size_t nnz=one_size/SIZE_OF_UNIT;
    while(!shared.add(ukey, one_column, nnz)){}          
  }
}




class feature_generator: public Reader{
public:
  feature_generator(const feature_generator &r):
      Reader(r)
    {}
	feature_generator(Shared_object &_s, options &_p):
      Reader(_s,_p)
    {}
  void open(){
    struct stat st;
    if(stat(p.train_kch_path.c_str(),&st)==1){
      cout << p.train_kch_path.c_str() << " does not exists" << endl;
      exit(1);
    }
    else
      cout << "read " << p.train_kch_path.c_str() << endl;
    file_db.open(p.train_kch_path.c_str(), HashDB::OREADER);
    cout << "kch file opened" << endl;

  }
	void operator()(){
    open();        
		steady_clock::time_point start, end;		
		start = std::chrono::steady_clock::now();
    cout << "clock started" << endl;

    int num_feat = shared.num_feature_;
    char* one_record_in_file=NULL;

    int buffer_size=10;
    map<string,string> columns_in_memory;
    vector<string> keys;
    shared.get_columns(keys, &columns_in_memory, buffer_size);
    map<string,string> generated_columns;
    size_t one_record_in_file_bytes;
    size_t generated_bytes;
    

    for(int key=0; shared.exit_flag == false;++shared.reader_iter){
      while(shared.stop_flag==true){ sleep(0.001); }
      key = (key) % num_feat + 1;
      // key begins with 1.
      ColIndexType colkey=static_cast<ColIndexType>(key);
//      ColIndexType ukey = key;	
      if(shared.memory_db.check((char*)&colkey, sizeof(ColIndexType))==-1){

        one_record_in_file = file_db.get((char*)&key, sizeof(int), &one_record_in_file_bytes);

        if(one_record_in_file != NULL){
          generated_columns.clear();
          generate(key, one_record_in_file, one_record_in_file_bytes,
                   columns_in_memory, generated_columns, generated_bytes);
          shared.add_bulk(generated_columns, generated_bytes);
        }

      }

    }
		end = std::chrono::steady_clock::now();
		cout << "reader " << (static_cast<double>(std::chrono::duration_cast<milliseconds>(end-start).count()) / 1000)
         << " seconds" << endl;
	}

  void generate(int key,
                const char* one_record_in_file,
                const size_t one_record_in_file_bytes,
                map<string,string> &columns_in_memory,
                map<string,string> &generated_features,
                size_t &generated_bytes){

    ColIndexType colkey=static_cast<ColIndexType>(key);
    string key_string(reinterpret_cast<char*>(&colkey), sizeof(ColIndexType));
    string val_string(one_record_in_file, one_record_in_file_bytes);
    
    generated_features.emplace(key_string,val_string);
    generated_bytes = one_record_in_file_bytes;    

    return;
  }
};

#endif
