#ifndef _BINARYCLASS_H_
#define _BINARYCLASS_H_

typedef unsigned long int ColIndexType;
typedef unsigned int RowIndexType;
typedef ColIndexType WeightKeyType;
typedef double ValueType;

#define SIZE_OF_UNIT (sizeof(RowIndexType)+sizeof(ValueType)) // Used for ??

#endif
