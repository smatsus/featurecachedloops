#define _FILE_OFFSET_BITS 64

#include <thread>
#include <boost/tokenizer.hpp>

#include "binaryclass.hpp"
#include "option.hpp"
#include "reader.hpp"
#include "binaryclass_trainer.hpp"

void read_label(string& labelname, vector<int> &y);
void read_test(string &filename, vector<int> &y, vector<vector<ColIndexType> > &x_idx, vector<vector<ValueType> > &x_val);
size_t get_filesize(string filename);

int main(int argc, char *argv[]) {  
  options option(argc, argv);
  Shared_object shared(option);

  cout << "working on the dataset " << option.train_datasetname << endl;        
  read_label(option.train_label_path, shared.y);
  cout << "number of data: " << shared.num_data_ << endl;
  cout << "number of features: " << shared.num_feature_ << endl;

  size_t filesize = get_filesize(option.train_kch_path);
  filesize = (filesize >> 20) ; // M bytes 
  if(filesize == 0){
    filesize = 1;
    cout << "File size: <1 M bytes \n";
  }else{
    cout << "File size: " << filesize << " M bytes \n";
  }
  int working_set_max_size = min( static_cast<int>(2.0*option.param_memory_capacity/filesize * shared.num_feature_),
                                  static_cast<int>(shared.num_feature_));
// Upperbound for working set (we don't expect to fulfill it)
  cout << "Cache:  " << option.param_memory_capacity  << "M bytes\n";
  cout << "Cachesize tuning:  " << working_set_max_size  << "\n";

  string test_filename = option.train_path + "/" + option.train_datasetname + ".test.txt" ;  
  size_t test_filesize = get_filesize(test_filename);
  if(test_filesize > 0){
    cout << "test file found" << endl;
    read_test(test_filename, shared.ytest, shared.Xtest_index, shared.Xtest_value);
    shared.have_test=true;
  }
  else{
    cout <<"test file not found" <<endl;
    shared.have_test=false;
  }

    
  if(option.debug){    
    shared.set_memory_db(shared.num_feature_, filesize);

    feature_generator r(shared, option);
    thread reading_thread(r);        
    while(shared.reader_iter < shared.num_data_){}
    shared.exit_flag=true;
    reading_thread.join();
    cout << "reading finished." << endl;
      
    shared.exit_flag=false;        
    binaryclass_trainer t(shared, option);
    thread training_thread(t);
    training_thread.join();    
  }else{
    shared.set_memory_db(working_set_max_size, option.param_memory_capacity);

    reader r(shared, option);
    thread reading_thread(r);        
    binaryclass_trainer t(shared, option);
    thread training_thread(t);

    reading_thread.join();
    training_thread.join();          
  }
    
  return 0;
}

void read_label(string& labelname, vector<int> &y){
	string fpath = labelname;
	ifstream ifs(fpath.c_str());	
	if(ifs.is_open()==false){
		cout << "cannot open label file:" << fpath<< endl;
		exit(1);
	}
	else
    cout << "read " << fpath << flush;
  
	string buf;
	for(int i=0;ifs && getline(ifs,buf); ++i ){
		y[i] = kyotocabinet :: atoi( &(buf[0]) );
    if(y[i] == 0 )
      y[i] = -1;
	}
  cout << "..finished." <<endl; 
}
void read_test(string &filename, vector<int> &y, vector<vector<ColIndexType> > &x_idx, vector<vector<ValueType> > &x_val){
  y.clear();
  x_idx.clear();
  x_val.clear();  
  ifstream ifs;
  ifs.open(filename, ifstream::in);        
  string line;
  while(getline(ifs, line)){
    boost::char_separator<char> sep(" \t:");
    boost::tokenizer<boost::char_separator<char>> tokens(line, sep);
    size_t count = 0;
    x_idx.push_back(vector<ColIndexType>());
    x_val.push_back(vector<ValueType>());
    for (const auto& t : tokens){
      count++;
      if(count == 1){
        int yi = stoul(t);
        if(yi == 0)
          yi = -1;
        y.push_back(yi);
        continue;
      }
      if(count%2){
        ValueType val = stod(t);
        x_val.back().push_back(val);
      }else{
        ColIndexType idx = stoul(t);
        x_idx.back().push_back(idx);
      }
    }
  }
}
size_t get_filesize(string filename){
  struct stat filestatus;
  stat(filename.c_str(), &filestatus);
  return filestatus.st_size;
}
