#ifndef _BINARY_TRAINER_H_
#define _BINARY_TRAINER_H_

#include "binaryclass_weight.hpp"
#include "trainer.hpp"

using namespace std;

class binaryclass_trainer : public trainer {

  binaryclass_weight w;
  vector<ValueType> expwTx;
  vector<int> y;
  double C;

public:
  binaryclass_trainer(const binaryclass_trainer &r):
      trainer(r),
      expwTx(r.expwTx),
      y(r.y),
      C(r.C)
    {}
	binaryclass_trainer(Shared_object &s, options &_p):
      trainer(s,_p),
      expwTx(l,1.0),
      y(s.y),
      C(p.param_C)
    {}
  double calc_obj(){
    double log_loss=0.0; // sum_i log ( 1.0 +exp (-yiwTxi) )
	 	for(int i=0;i<l;++i)
      log_loss += logloss(y[i], expwTx[i]);    
    return log_loss * C + w.l1_norm(); 
  }    
  void show_statistics(){
    cout << " iteration " << shared.trainer_iter / n
         << " iteration_reader " << shared.reader_iter / n
         << " num update " << num_update
         << " num evict " << num_evict << endl;         
    cout << " #nnz/#features " << w.size() << " / " << n
         << " #memory_db_size " << shared.memory_size() << " MB"
         << " #working_set " << shared.working_set_size
         << " #trainer_access/#reader_access " << num_access << " / " << shared.reader_iter << " = " << 1.* num_access / shared.reader_iter << endl;
    cout << " obj=" << setprecision(9) << calc_obj()
         << " |w|=" << w.l1_norm()
         << " Gmax=" << Gmax_old
         << " Gmax_rel=" << Gmax_old / Gmax_init
         << " accummulated time: " << DURATION(steady_clock::now()-start) << " seconds" << endl;
    if(shared.have_test){
      double accuracy=0.0;
      eval_test(accuracy);
      cout << "test_accuracy " << accuracy << endl;
    }
  }
  double logloss(const int yi, const double exp_u){
    if(yi == 1)// log (1 + exp(-ui) ) = log ( (1 + exp_u)/exp_u ) 
      return log(1.0 + 1.0 / exp_u );
    else  // log (1 + exp_u )
      return log(1.0 + exp_u);
  }
  double difflogloss(const int yi, const double exp_u, const double d){
    double exp_u_plus_d = exp_u * exp(d);
    if(yi == 1) 
      return log( (1.0 + 1.0 / exp_u ) / (1.0 + 1.0 / exp_u_plus_d ) );
    else  // log (1 + exp_u ) -  log (1 + exp(u+d) ) 
      return log( (1.0 + exp_u) / (1.0 + exp_u_plus_d ) );    
  }
  void initialize(){
  }
  void solve (ValueType &new_w_j, const ValueType old_w_j, const double g, const double h){
    // solve min |w_j| + g (w_j-w_j_old) + 0.5 h(w_j-w_j_old)^2
    // ==> sgn(w_j) + g + h(w_j-old_w_j) =0
    //
    // if  -1 < g + h ( 0.0 - old_w_j) < 1 
    //  w_j = 0.0
    // else if g + h ( 0.0 - old_w_j) >= 1 
    //  w_j = old_w_j - (g - 1)/h
    // else if g + h ( 0.0 - old_w_j) <= -1 
    //  w_j = old_w_j - (g + 1)/h    
    if( (g + h * ( 0.0 - old_w_j) < 1.0)
        && (g + h * ( 0.0 - old_w_j) > -1.0) )
      new_w_j = 0.0;
    else if ( !(g + h * ( 0.0 - old_w_j) < 1.0) )
      new_w_j = old_w_j - (g - 1.0)/h;
    else if ( !(g + h * ( 0.0 - old_w_j) > -1.0) )
      new_w_j = old_w_j - (g + 1.0)/h;

    new_w_j = min(max(new_w_j , old_w_j-10.0),old_w_j+10.0); //SHIN::??

  }
    
  // void optimize_with_Lipschitz_constant(const ColIndexType j, const ValueType old_w_j,
  //               ValueType &new_w_j, bool &remove,
  //               const char* column, const size_t nnz){
  //   // L = C \sum log(1 + exp(-wTyixi))
  //   // nabla_j L = -C \sum yi*xij/(1 + exp(wTxiyi))
  //   //           = -C {\sum xij/(1 + exp(wTxi))
  //   //                   - \sum{i:yi=-1} xij [1/(1 + exp(wTxi)) + 1/(1 + exp(-wTxi))]  }
  //   //           = -C {\sum xij/(1 + exp(wTxi)) - \sum{i:yi=-1} xij  }

  //   // L <= L_upper = L0 + nabla_j L * (w-w0) + 0.25 C \sum xij^2 (w-w0)^2
  //   //
  //   //  w - w0 = -nabla_j L / (0.5 xij^2)

  //   // min nabla_j L * (w-w0) + 0.25 C sum_i xij^2 (w-w0)^2 + |w|   (-> w = Prox( -nabla_j L / (0.5 xij^2) ) )
  //   // =  solve (w, w0, nabla_j L, 2 C sum_i xij^2 )
  //   // 
  //   // solve min |w_j| + g (w_j-w_j_old) + 0.5 h(w_j-w_j_old)^2

  //   double nabla_j = 0.0; // = -C \sum yi*xij/(1 + exp(wTxiyi))
  //   double h_jj = 0.0;
  //   double xj_sum_negative=0.0; // sum_{i:y[i] = -1} xij

  //   for(int offset=0; offset<nnz*SIZE_OF_UNIT; offset+=SIZE_OF_UNIT){
  //     const RowIndexType i
  //       = *reinterpret_cast<const RowIndexType*>(&column[offset])-1;
  //     const ValueType xij
  //       = *reinterpret_cast<const ValueType*>(&column[offset+sizeof(RowIndexType)]);
  //     nabla_j += xij / ( 1.0 + expwTx[i] ) ;
  //     h_jj += C * xij * xij / 4.0;
  //     if(y[i] == -1)
  //       xj_sum_negative += xij;
  //   }
  //   nabla_j = -C * (nabla_j - xj_sum_negative);

  //   double Gp = nabla_j+1;
  //   double Gn = nabla_j-1;
  //   double PG = 0;
  //   if(old_w_j == 0)
  //   {
  //     if(Gp < 0)
  //       PG = -Gp;
  //     else if(Gn > 0)
  //       PG = Gn;
  //     else if(Gp > Gmax_old/l && Gn < -Gmax_old/l)
  //     {
  //       new_w_j = 0.0;
  //       remove = true;
  //       return;
  //     }
  //   }
  //   else if(old_w_j > 0)
  //     PG = fabs(Gp);
  //   else
  //     PG = fabs(Gn);
    
  //   Gmax_new = max(Gmax_new, PG);

  //   solve(new_w_j,old_w_j,nabla_j,h_jj);

  //   return;
  // }
    void optimize(const ColIndexType j, const ValueType old_w_j,
                ValueType &new_w_j, bool &remove,
                const char* column, const size_t nnz){
    // L = C \sum log(1 + exp(-wTyixi))
    // nabla_j L = -C \sum yi*xij/(1 + exp(wTxiyi))
    //           = -C {\sum xij/(1 + exp(wTxi))
    //                   - \sum{i:yi=-1} xij [1/(1 + exp(wTxi)) + 1/(1 + exp(-wTxi))]  }
    //           = -C {\sum xij/(1 + exp(wTxi)) - \sum{i:yi=-1} xij  }
    // hessian_jj L = C \sum xij^2 * exp(wTxiyi)/(1 + exp(wTxiyi))^2
    //                C \sum xij^2 / ( (1 + exp(wTxiyi)) * (1 + exp(-wTxiyi)) )

    double nabla_j = 0.0; // = -C \sum yi*xij/(1 + exp(wTxiyi))
    double h_jj =0.0;
    double xj_sum=0.0; //  sum_i xij
    double xj_sum_negative=0.0; // sum_{i:y[i] = -1} xij
    double xj_sum_positive=0.0; 
    double xj_max=0.0; // max_i xij

    for(int offset=0; offset<nnz*SIZE_OF_UNIT; offset+=SIZE_OF_UNIT){
      const RowIndexType i
        = *reinterpret_cast<const RowIndexType*>(&column[offset])-1;
      const ValueType xij
        = *reinterpret_cast<const ValueType*>(&column[offset+sizeof(RowIndexType)]);
//      cout << i << ":" << xij  <<  " "; 
      nabla_j += xij / ( 1.0 + expwTx[i] ) ;
      h_jj += C * xij * xij / ( 2.0 + expwTx[i] + 1.0 / expwTx[i]);
      xj_sum += xij;
      if(y[i] == -1)
        xj_sum_negative += xij;
      if(xj_max < xij)
        xj_max = xij;
    }
//    cout <<endl;
    nabla_j = -C * (nabla_j - xj_sum_negative);  
    xj_sum_positive = xj_sum - xj_sum_negative;  // sum_{i:y[i] =+1} xij  
    if(xj_max <= 0.0){
      cout << "Assumption that max_i xij >= 0 violated.";
      exit(1);
    }
    if(_print){
      cout << "L1 " << nabla_j << " L2 " << h_jj <<endl; 
    }
      
    double Gp = nabla_j+1;
    double Gn = nabla_j-1;
    double PG = 0;
    if(old_w_j == 0)
    {
      if(Gp < 0)
        PG = -Gp;
      else if(Gn > 0)
        PG = Gn;
      else if(Gp > Gmax_old/l && Gn < -Gmax_old/l)
      {
        new_w_j = 0.0;
        remove = true;
        return;
      }
    }
    else if(old_w_j > 0)
      PG = fabs(Gp);
    else
      PG = fabs(Gn);
    
    Gmax_new = max(Gmax_new, PG);

    solve(new_w_j,old_w_j,nabla_j,h_jj);
    if(fabs(new_w_j -old_w_j)<10e-12)
      return;

    double expected_progress = -nabla_j * (new_w_j-old_w_j) + (fabs(old_w_j) - fabs(new_w_j));
    if(_print){
      cout << "expected_progress " << expected_progress << endl;
    }
    for(size_t num_try=0; num_try < 10; ++num_try){
      double exp_d_xjmax = exp((new_w_j-old_w_j)*xj_max);
      double Csumxij_over_1pexpwTxi = -nabla_j + C*xj_sum_negative;
    // nabla_j L = -C \sum yi*xij/(1 + exp(wTxiyi))
    //           = -C {\sum xij/(1 + exp(wTxi))
    //                   - \sum{i:yi=-1} xij [1/(1 + exp(wTxi)) + 1/(1 + exp(-wTxi))]  }
    //           = -C {\sum xij/(1 + exp(wTxi)) - \sum{i:yi=-1} xij  }
      double Csumxij_over_1pexpmwTxi = nabla_j + C*xj_sum_positive;
    // nabla_j L = -C \sum yi*xij/(1 + exp(wTxiyi))
    //           = -C {\sum -xij/(1 + exp(-wTxi))
    //                   + \sum{i:yi=+1} xij [1/(1 + exp(-wTxi)) + 1/(1 + exp(wTxi))]  }
    //           = -C {\sum -xij/(1 + exp(-wTxi)) + \sum{i:yi=+1} xij  }
      
//  check if lower_bounds of objective function difference exceeds expected one
      double lower_bound; // on the objective function difference 
      lower_bound = -( log(1+Csumxij_over_1pexpwTxi*(exp_d_xjmax-1)/xj_max/(C*nnz))*C*nnz
                     + fabs(new_w_j) - fabs(old_w_j) - (new_w_j-old_w_j)*(xj_sum_positive) );
      if(_print){  
        cout << "lower bound " << lower_bound  << " sum1 " << Csumxij_over_1pexpwTxi  <<endl;
      }
      if(lower_bound > 0.01 * expected_progress)
        break;
      
      lower_bound = -(log(1+Csumxij_over_1pexpmwTxi*(1/exp_d_xjmax-1)/xj_max/(C*nnz))*C*nnz 
                      + fabs(new_w_j) - fabs(old_w_j) + (new_w_j-old_w_j)*(xj_sum_negative) );
      if(_print){
         cout << "lower bound " << lower_bound  << " sum2 " << Csumxij_over_1pexpmwTxi  <<endl;
      }
      if(lower_bound > 0.01 * expected_progress)
        break;
      
      double obj_diff = 0.0;
      obj_diff -= fabs(new_w_j) - fabs(old_w_j);
      for(int offset=0; offset<nnz*SIZE_OF_UNIT; offset+=SIZE_OF_UNIT){
        const RowIndexType i
          = *reinterpret_cast<const RowIndexType*>(&column[offset])-1;
        const ValueType xij
          = *reinterpret_cast<const ValueType*>(&column[offset+sizeof(RowIndexType)]);                        
        double diff_log_loss_i = difflogloss(y[i], expwTx[i], (new_w_j-old_w_j)*xij );
        obj_diff += C*diff_log_loss_i; //(new_log_loss_i - old_log_loss_i);                
      }
      if(_print){
        cout << "obj_diff " << obj_diff <<endl;
      }
      if(obj_diff > 0.01 * expected_progress)
        break;
      h_jj *= 2.0;
      expected_progress *= 0.5;
      solve(new_w_j,old_w_j,nabla_j,h_jj);
    }        
  }
  void subroutine(char* one_column){
    size_t nnz;
    int key_for_access=0;
    ColIndexType j=0;
    while(shared.get_column(key_for_access,j,one_column,nnz) == false){
      sleep(0.00001);
    }
    ++num_access;
    ValueType w_j = w.get(j);
    ValueType new_w_j;
    bool remove=false;
    optimize(j,w_j,new_w_j,remove,one_column,nnz);
    if(!p.debug && remove){
      if(shared.remove_column(j,key_for_access)){
        ++num_evict;
        return;
      }
      else{
        // already removed by reader
        return;
      }
    }
    if(fabs(new_w_j-w_j)<10e-12)
      return;
    _print=false;

    for(int offset=0; offset<nnz*SIZE_OF_UNIT; offset+=SIZE_OF_UNIT){
      RowIndexType i
        = *reinterpret_cast<RowIndexType*>(one_column+offset)-1;
      ValueType xij
        = *reinterpret_cast<ValueType*>( one_column+offset+sizeof(RowIndexType) );
      expwTx[i] *= exp(xij * (new_w_j-w_j));
    }
    w.set(j,new_w_j);
    num_update++;    
    
  }
  
    
 void  eval_test(double &accuracy){
    int num_true=0;
    for(size_t i=0; i<shared.ytest.size(); ++i){
      ValueType xiTw=0.0;
      ValueType w_j=0.0;
      for(size_t nnzitr=0; nnzitr< shared.Xtest_index[i].size(); ++nnzitr){
        ColIndexType j = shared.Xtest_index[i][nnzitr];
        ValueType x_ij = shared.Xtest_value[i][nnzitr];
        ValueType w_j = w.get(j);
        xiTw += w_j * x_ij;          
      }
      double yi = shared.ytest[i];
      if(yi * xiTw > 0)
        num_true++;
    }    
    accuracy = 100.0 *((double) num_true) / shared.ytest.size();
    return;
  }
    
};

#endif
