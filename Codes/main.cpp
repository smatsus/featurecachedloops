#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <thread>
#include <boost/program_options.hpp>

#include "tbb/task_scheduler_init.h"
#include "tbb/compat/thread"
#include "tbb/tick_count.h"

#define _FILE_OFFSET_BITS 64

#include "binaryclass.hpp"
#include "option.hpp"
#include "reader.hpp"
#include "trainer.hpp"

using namespace tbb;
using namespace std;
using namespace boost::program_options;

void read_label(string& labelname, vector<int> &y){
	string fpath = labelname;
	ifstream ifs(fpath.c_str());	
	if(ifs.is_open()==false){
		cout << "cannot open label file:" << fpath<< endl;
		exit(1);
	}
	else
    cout << "read " << fpath << flush;
  
	string buf;
	for(int i=0;ifs && getline(ifs,buf); ++i ){
		y[i] = kyotocabinet :: atoi( &(buf[0]) );
    if(y[i] == 0 )
      y[i] = -1;
	}
  cout << "..finished." <<endl; 
}


int main(int argc, char *argv[]) {  
  options option(argc, argv);
  Shared_object shared(option);
  cout << "working on the dataset " << option.train_datasetname << endl;        
  read_label(option.train_label_path, shared.y);
  cout << "number of data: " << shared.l << endl;
  cout << "number of features: " << shared.n << endl;

  struct stat filestatus;
  stat(option.train_kch_path.c_str(), &filestatus);
  size_t filesize = (filestatus.st_size >> 20) ;
  if(filesize == 0){
    filesize = 1;
    cout << "File size: <1 M bytes \n";
  }else{
    cout << "File size: " << filesize << " M bytes \n";
  }
  int working_set_max_size = min( static_cast<int>(2.0*option.param_memory_capacity/filesize * shared.n),(int) shared.n);
// Upperbound for working set (we don't expect to fulfill it)
  cout << "Cache:  " << option.param_memory_capacity  << "M bytes\n";
  cout << "Cachesize tuning:  " << working_set_max_size  << "\n";

    
  if(option.debug){    
    shared.set_memory_db(shared.n, filesize);

    feature_generator r(shared, option);
    std::thread reading_thread(r);        
    while(shared.reader_iter < shared.l){}
    shared.exit_flag=true;
    reading_thread.join();
    cout << "reading finished." << endl;
      
    shared.exit_flag=false;        
    trainer t(shared, option);
    std::thread training_thread(t);
    training_thread.join();    
  }else{
    shared.set_memory_db(working_set_max_size, option.param_memory_capacity);

    reader r(shared, option);
    std::thread reading_thread(r);        
    trainer t(shared, option);
    std::thread training_thread(t);

    reading_thread.join();
    training_thread.join();          
  }
    
  return 0;
}

