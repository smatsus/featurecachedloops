#ifndef _MULTICLASS_TRAINER_H_
#define _MULTICLASS_TRAINER_H_

#include "multiclass_weight.hpp"

#include "trainer.hpp"

using namespace std;

class multiclass_trainer : public trainer{

  size_t num_class_;
  
public:
  multiclass_weight w;
  vector<vector<ValueType> > expwTx;
  vector<int> y;
  double C;
  multiclass_trainer(const multiclass_trainer &r):
      trainer(r),
      num_class_(r.num_class_),
      w(r.w),
      expwTx(r.expwTx),
      y(r.y),
      C(r.C)
    {}
	multiclass_trainer(Shared_object &s, options &_p):
      trainer(s,_p),
      num_class_(s.num_class_),
      w(s.num_class_),
      expwTx(l,vector<ValueType>(num_class_,1.0)),
      y(s.y),
      C(p.param_C)
    {}
  double calc_obj(){
    // Calc |w| + sum - wTx.y + log sum exp wTx.k
    double loss=0.0;
    for(int i=0;i<l;++i)
      loss += -log(expwTx[i][y[i]]) + logsum(expwTx[i]);    
    return loss * C + w.l1_norm();
  }
  double logsum(const vector<ValueType> &expwTxi){
    double sum=0.0;
    for(size_t k=0; k<num_class_; ++k){
      sum += expwTxi[k];
    }
    return log(sum);
  }
  void eval_test(double &macro_f1, double &micro_f1){

    vector<int> true_positive(num_class_,0);
    vector<int> false_positive(num_class_,0);
    vector<int> false_negative(num_class_,0);

    for(size_t i=0; i<shared.ytest.size(); ++i){
      vector<ValueType> xiTw(num_class_,0.0);
      vector<ValueType> w_j(num_class_,0.0);
      for(size_t nnzitr=0; nnzitr< shared.Xtest_index[i].size(); ++nnzitr){
        ColIndexType j = shared.Xtest_index[i][nnzitr];
        ValueType x_ij = shared.Xtest_value[i][nnzitr];
        w.get(j, w_j);
        for(size_t k=0; k<num_class_; ++k){
          xiTw[k] += w_j[k] * x_ij;          
        }
      }

      int hatyi = 0;
      ValueType max_score = xiTw[0];
      
      for(size_t k=0; k<num_class_; ++k){        
        ValueType wkTxi = xiTw[k];
        if(wkTxi > max_score){
          max_score = wkTxi;
          hatyi = k;
        }                
      }
      int yi = shared.ytest[i];
      if(yi == hatyi){
        true_positive[hatyi]++;
      }else{
        false_positive[hatyi]++;
        false_negative[yi]++;
      }
    }

    int nr_precision=0;
    int dr_precision=0;
    int dr_recall=0;
    
    for(size_t k = 0; k < num_class_; ++k){
      nr_precision += true_positive[k];
      dr_precision += true_positive[k] + false_positive[k];
      dr_recall += true_positive[k] + false_negative[k];
      
      ValueType p = 1.0;
      ValueType r = 1.0;
      
      if(true_positive[k]+false_positive[k] > 0)
        p = (ValueType)true_positive[k]/(true_positive[k] + false_positive[k]);          
      if(true_positive[k]+false_negative[k] > 0)
        r = (ValueType)true_positive[k]/(true_positive[k] + false_negative[k]);
      
      if(p or r)
        macro_f1 += (2*p*r)/(p+r);
    }
    macro_f1 /= num_class_;
    ValueType p = (ValueType) nr_precision / dr_precision; 
    ValueType r = (ValueType) nr_precision / dr_recall;
    if(p or r)
      micro_f1 = (2 * p * r) / (p + r);
    return;
    
  }
  
  void show_statistics(){
    
    cout << " iteration " << shared.trainer_iter / n
         << " iteration_reader " << shared.reader_iter / n
         << " num update " << num_update
         << " num evict " << num_evict << endl;         
    cout << " #nnz/#features " << w.size() << " / " << n
         << " #memory_db_size " << shared.memory_size() << " MB"
         << " #working_set " << shared.working_set_size
         << " #trainer_access/#reader_access " << num_access << " / " << shared.reader_iter << " = " << 1.* num_access / shared.reader_iter << endl;
    cout << " obj=" << setprecision(9) << calc_obj()
         << " |w|=" << w.l1_norm()
         << " Gmax=" << Gmax_old
         << " Gmax_rel=" << Gmax_old / Gmax_init
         << " accummulated time: " << DURATION(steady_clock::now()-start) << " seconds" << endl;
    if(shared.have_test){
      double macro_f1=0.0;
      double micro_f1=0.0;
      eval_test(macro_f1, micro_f1);
      cout << " macro_f1 " << macro_f1 << " micro_f1 " << micro_f1 << endl;      

    }
//    w.print();
  }
  double diff_logsumexp(const vector<ValueType> &expwTxi,
                        const vector<ValueType> &neww_j,
                        const vector<ValueType> &oldw_j,
                        const ValueType xij
                        ){
    double newsumexp=0.0;
    double oldsumexp=0.0;
    for(size_t k=0;k<num_class_;++k){
      if(fabs((neww_j[k] -oldw_j[k])) < 1.0e-12)
        continue;
      oldsumexp += expwTxi[k]; 
      newsumexp += expwTxi[k]* exp( (neww_j[k] - oldw_j[k]) * xij );
    }
    return log( newsumexp / oldsumexp );
  }
  void initialize(){
  }

  


  void _subroutine(char* one_column){
    size_t nnz=0;
    int key_for_access=0;
    ColIndexType j=0;
    while(shared.get_column(key_for_access,j,one_column,nnz) == false){
      sleep(0.00001);
    }
    ++num_access;
    if(_print){
      cout << "j " << j <<endl;
    }
    vector<ValueType> w_j(num_class_);
    w.get(j, w_j);
    vector<ValueType> new_w_j(num_class_);
    bool remove=false;
    
    optimize(j,new_w_j,w_j,one_column,nnz,remove);
    
    if(!p.debug && remove){
      if(shared.remove_column(j,key_for_access)){
        ++num_evict;
        return;
      }
      else{
        // already removed by reader
        return;
      }
    }
    for(int offset=0; offset<nnz*SIZE_OF_UNIT; offset+=SIZE_OF_UNIT){
      RowIndexType i
        = *reinterpret_cast<RowIndexType*>(&one_column[offset])-1;
      ValueType xij
        = *reinterpret_cast<ValueType*>(&one_column[offset+sizeof(RowIndexType)]);
      for(size_t k=0; k<num_class_; ++k){
        if(fabs(new_w_j[k]-w_j[k])<10e-12)
          continue;
        expwTx[i][k] *= exp(xij * (new_w_j[k]-w_j[k]));
      }
    }
    w.set(j,new_w_j);
    num_update++;
    _print=false;
    
  }

  void subroutine(char* one_column){
    size_t nnz=0;
    int key_for_access=0;
    ColIndexType j=0;
    while(shared.get_column(key_for_access,j,one_column,nnz) == false){
      sleep(0.00001);
    }
    ++num_access;
    if(_print){
      cout << "j " << j <<endl;
    }
    vector<ValueType> w_j(num_class_);
    w.get(j, w_j);
//    vector<ValueType> new_w_j(num_class_);
    bool remove=false;

    vector<ValueType> step(num_class_);
    vector<ValueType> nabla_j(num_class_,0.0);
    vector<ValueType> h_jj(num_class_,0.0);


    calc_diff_(one_column, nnz, nabla_j, h_jj);
    if(check_evict(j, key_for_access, w_j, nabla_j, Gmax_new, Gmax_old)==true )
      return;
    
    calc_firststep(step, w_j, nabla_j, h_jj);
    
    if( calc_maxstep(step) < 1.0e-12 )
      return;
    
    calc_stepsize(one_column, nnz, step, w_j, nabla_j);
//    calc_wj(one_column, nnz, step, w_j);
		// for(int k=0;k< num_class_;k++){
		// 	if(fabs(step[k])>1.0e-12){
		// 		w_j[k] += step[k];
    //     for(int offset=0; offset<nnz*SIZE_OF_UNIT; offset+=SIZE_OF_UNIT){
    //       RowIndexType i
    //         = *reinterpret_cast<RowIndexType*>(&one_column[offset])-1;
    //       ValueType xij
    //         = *reinterpret_cast<ValueType*>(&one_column[offset+sizeof(RowIndexType)]);
		// 			expwTx[i][k] *= exp( xij * step[k] );
		// 		}
		// 	}
		// }

    for(int offset=0; offset<nnz*SIZE_OF_UNIT; offset+=SIZE_OF_UNIT){
      RowIndexType i
        = *reinterpret_cast<RowIndexType*>(&one_column[offset])-1;
      ValueType xij
        = *reinterpret_cast<ValueType*>(&one_column[offset+sizeof(RowIndexType)]);
      for(size_t k=0; k<num_class_; ++k){
        if(fabs(step[k])>10e-12){
          expwTx[i][k] *= exp( xij * step[k] );
        }
      }
    }
    for(size_t k=0; k<num_class_; ++k){
      if(fabs(step[k])>10e-12)
        w_j[k] += step[k];
    }
    w.set(j,w_j);

    num_update++;
    _print=false;
    
  }
  void calc_diff_(char* column, size_t nnz, vector<double> &nabla_j, vector<double> &h_jj){

    for(int offset=0; offset<nnz*SIZE_OF_UNIT; offset+=SIZE_OF_UNIT){
      const RowIndexType i
        = *reinterpret_cast<const RowIndexType*>(&column[offset])-1;
      const ValueType xij
        = *reinterpret_cast<const ValueType*>(&column[offset+sizeof(RowIndexType)]);

      nabla_j[y[i]] -= xij;        
      
      double sumexpwTxi=0.0;
      for(size_t k=0; k<num_class_; ++k){
        sumexpwTxi += expwTx[i][k];
      }
      for(size_t k=0; k<num_class_; ++k){
        const ValueType tmp = expwTx[i][k] / sumexpwTxi ;
        nabla_j[k] += xij * tmp;
        h_jj[k] += xij * xij * tmp * (1.0 - tmp);
        // nabla_j[k] += xij * expwTx[i][k] / sumexpwTxi ;
        // h_jj[k] += xij * xij * (expwTx[i][k] / sumexpwTxi)
        //                      * (1.0 - expwTx[i][k] / sumexpwTxi );
      }
    }
    for(size_t k=0; k<num_class_; ++k){
      nabla_j[k] *= C;
      h_jj[k] *= C;
    }


  }

  void calc_diff(char* one_column, size_t nnz, vector<double> &L1, vector<double> &L2){
    // l = <w,f(xi,yi)> - log sum exp <w,f(xi,m)>
    // L1[m] = sum_i nabla log sum exp <w,f(xi,m)> = sum_i sum xij exp <w,f(xi,m)> / sum exp <w,f(xi,m)> )
    // L2[m] = sum_i nabla nabla log sum exp <w,f(xi,m)>

    int M =num_class_;
		for(int m=0;m<M;++m){
			L1[m] = 0.0;
			L2[m] = 0.0;
		}
    
    for(int offset=0; offset<nnz*SIZE_OF_UNIT; offset+=SIZE_OF_UNIT){
      RowIndexType i
        = *reinterpret_cast<RowIndexType*>(&one_column[offset])-1;
      ValueType xij
        = *reinterpret_cast<ValueType*>(&one_column[offset+sizeof(RowIndexType)]);

			double normal_sum=0.0;
			for(int m=0;m<M;++m)
        normal_sum += expwTx[i][m];
      
      L1[y[i]] -= xij;
			for(int m=0;m<M;++m){
				double temp2 = expwTx[i][m]/normal_sum;				
				L1[m] += xij * temp2;
				L2[m] += (xij * xij) * temp2*(1.0-temp2);
			}
		}

    for(int m=0;m<M;++m){
			L1[m] = C * L1[m];
			L2[m] = C * L2[m];
		}
	}

	double calc_maxstep(vector<double> &step){
    int M= num_class_;
		double ans = 0.0;
		for(int m=0;m<M;++m){
			if(ans<fabs(step[m]))
        ans = fabs(step[m]);
		}
		return ans;
	}

  
	bool check_evict(ColIndexType j, ColIndexType key_for_access, vector<double> &w_j, vector<double>  &L1, double &max_v, double prev_v){
    int M =num_class_;
		int count = 0;
		for(int m=0; m<M; ++m){
			double vj_m;
			double Gp_m = L1[m] + 1.0;
			double Gn_m = L1[m] - 1.0;
			
			if(w_j[m] == 0){
				if(Gp_m < 0)
          vj_m = -Gp_m;
				else if(Gn_m > 0)
          vj_m = Gn_m;
				else if(Gp_m > (prev_v/l) && Gn_m < -(prev_v/l) ){
					count++;
					vj_m = 0.0;
				}
				else
          vj_m = 0.0;
			}
			else if(w_j[m] > 0)
        vj_m = fabs(Gp_m);
			else
        vj_m = fabs(Gn_m);

			max_v = max(max_v, vj_m);
		}
		if(count==M && !p.debug)
			return shared.remove_column(j,key_for_access);
		else
      return false;
	}
	void calc_firststep(vector<double> &step, vector<double> &w_j, vector<double> &L1, vector<double> &L2){    
    int M =num_class_;
		for(int m=0;m<M;++m){
			if( (L1[m] + 1.0) <= w_j[m] * L2[m] )
        step[m] = -(L1[m] + 1.0) / (L2[m]);
			else if( (L1[m] - 1.0) >= w_j[m] * L2[m] )
        step[m] = -(L1[m] - 1.0) / (L2[m]);
			else
        step[m] = -w_j[m];

			step[m] = min(max(step[m],-10.0),10.0); //???
		}
	}
	void calc_stepsize(char* one_column, size_t nnz, vector<double> &step, vector<double> &w_j, vector<double> &L1){
    int M =num_class_;
		int iter_count = 0;
		double delta = 0.0;
		for(int m=0;m<M;++m){
			if(fabs(step[m])<1.0e-12)
        continue;
			delta += L1[m] * step[m] + fabs( w_j[m] + step[m] ) - fabs(w_j[m]);
		}
		double cond = 0.0;
		double temp = 0.0;
    double temp2 = 0.0;

    double	sigma = 0.01;

		while(true){
			temp = 0.0;
			for(int m=0;m<M;++m){
				if(fabs(step[m])<1.0e-12)
          continue;	
				temp += fabs(w_j[m]+step[m])-fabs(w_j[m]);
			}
			cond = temp - sigma * delta;
			
			temp = 0.0;
      for(int offset=0; offset<nnz*SIZE_OF_UNIT; offset+=SIZE_OF_UNIT){
        RowIndexType i
          = *reinterpret_cast<RowIndexType*>(&one_column[offset])-1;
        ValueType xij
          = *reinterpret_cast<ValueType*>(&one_column[offset+sizeof(RowIndexType)]);
				temp += step[y[i]] * xij;	
			}
			cond = cond - C * temp;

/*			double tmp = exp(step*xjmax);
			appxcond1 = log(1+sum1*(tmp-1)/xjmax/csum)*csum + cond - step*psum;
			appxcond2 = log(1+sum2*(1/tmp-1)/xjmax/csum)*csum + cond + step*nsum;
			if(min(appxcond1,appxcond2) <= 0)	break;*/
      for(int offset=0; offset<nnz*SIZE_OF_UNIT; offset+=SIZE_OF_UNIT){
        RowIndexType i
          = *reinterpret_cast<RowIndexType*>(&one_column[offset])-1;
        ValueType xij
          = *reinterpret_cast<ValueType*>(&one_column[offset+sizeof(RowIndexType)]);
				temp=0.0;
        temp2=0.0;
				for(int m=0;m<M;++m){
					if(fabs(step[m])<1.0e-12)
            continue;
					temp2 += expwTx[i][m];
          temp += expwTx[i][m] * exp(step[m]*xij);
				}
				cond += C * log(temp/temp2);
			}

/*			for(int k=0;k<d.nnz;++k){
				int i = d.idx[k]-1;
				double exp_dx = exp( step * d.val[k]);
				cond += C * log( (1.0+u[i]*exp_dx) / (exp_dx+u[i]*exp_dx) );
			}*/
			iter_count++;

			if(cond < 0)
        break;
			if(iter_count >= 100){
//				cout << "#" << flush;
				break;
			}
			for(int m=0;m<M;++m)
        step[m] *= 0.5;
			delta *= 0.5;
		}
	}
	void calc_wj(char* one_column, size_t nnz, vector<double> &step, vector<double> &w_j){
		for(int k=0;k< num_class_;k++){
			if(fabs(step[k])>1.0e-12){
				w_j[k] += step[k];
        for(int offset=0; offset<nnz*SIZE_OF_UNIT; offset+=SIZE_OF_UNIT){
          RowIndexType i
            = *reinterpret_cast<RowIndexType*>(&one_column[offset])-1;
          ValueType xij
            = *reinterpret_cast<ValueType*>(&one_column[offset+sizeof(RowIndexType)]);
					expwTx[i][k] *= exp( xij * step[k] );
				}
			}
		}
	}
  


  
  void solve (ValueType &new_w_j, const ValueType old_w_j, const double g, const double h){
    // solve min |w_j| + g*(w_j-w_j_old) + 0.5 h*(w_j-w_j_old)^2
    // ==> sgn(w_j) + g + h(w_j-old_w_j) =0
    //
    // if  -1 < g + h ( 0.0 - old_w_j) < 1 
    //  w_j = 0.0
    // else if g + h ( 0.0 - old_w_j) >= 1 
    //  w_j = old_w_j - (g - 1)/h
    // else if g + h ( 0.0 - old_w_j) <= -1 
    //  w_j = old_w_j - (g + 1)/h    
    if( (g + h * ( 0.0 - old_w_j) < 1.0)
        && (g + h * ( 0.0 - old_w_j) > -1.0) )
      new_w_j = 0.0;
    else if ( !(g + h * ( 0.0 - old_w_j) < 1.0) )
      new_w_j = old_w_j - (g - 1.0)/h;
    else if ( !(g + h * ( 0.0 - old_w_j) > -1.0) )
      new_w_j = old_w_j - (g + 1.0)/h;

    new_w_j = min(max(new_w_j , old_w_j-10.0),old_w_j+10.0); //SHIN::??

  }
    
  void optimize(
    const ColIndexType j,
    vector<ValueType> &new_w_j,
    const vector<ValueType> &old_w_j,
    const char* column,
    const size_t nnz,
    bool &remove){
    
    // L = C \sum -wyTxi + log(sum_k exp(wkTxi))
    // nabla_jk L = C \sum - xij [[y==k]] +  xij / sum_k exp(-wkTxi))
    // //           = -C {\sum xij/(1 + exp(wTxi))
    // //                   - \sum{i:yi=-1} xij [1/(1 + exp(wTxi)) + 1/(1 + exp(-wTxi))]  }
    // //           = -C {\sum xij/(1 + exp(wTxi)) - \sum{i:yi=-1} xij  }
    // hessian_jjkk L = C \sum xij^2 * exp(wTxiyi)/(1 + exp(wTxiyi))^2
    //                C \sum xij^2 / ( (1 + exp(wTxiyi)) * (1 + exp(-wTxiyi)) )

    vector<ValueType> nabla_j(num_class_, 0.0); 
    vector<ValueType> h_jj(num_class_, 0.0);
    // double xj_sum=0.0; //  sum_i xij
    // double xj_sum_negative=0.0; // sum_{i:y[i] = -1} xij
    // double xj_sum_positive=0.0; 
    // double xj_max=0.0; // max_i xij

    for(int offset=0; offset<nnz*SIZE_OF_UNIT; offset+=SIZE_OF_UNIT){
      const RowIndexType i
        = *reinterpret_cast<const RowIndexType*>(&column[offset])-1;
      const ValueType xij
        = *reinterpret_cast<const ValueType*>(&column[offset+sizeof(RowIndexType)]);

      nabla_j[y[i]] -= xij;        
      
      double sumexpwTxi=0.0;
      for(size_t k=0; k<num_class_; ++k){
        sumexpwTxi += expwTx[i][k];
      }
      for(size_t k=0; k<num_class_; ++k){      
        nabla_j[k] += xij * expwTx[i][k] / sumexpwTxi ;
        h_jj[k] += xij * xij * (expwTx[i][k] / sumexpwTxi)
                             * (1.0 - expwTx[i][k] / sumexpwTxi );
      }
    }
    for(size_t k=0; k<num_class_; ++k){
      nabla_j[k] *= C;
      h_jj[k] *= C;
    }
    // if(xj_max <= 0.0){
    //   cout << "Assumption that max_i xij >= 0 violated.";
    //   exit(1);
    // }
    if(_print){
      cout << "L1 " << nabla_j[0] << " L2 " << h_jj[0] <<endl; 
      cout << "L1 " << nabla_j[1] << " L2 " << h_jj[1] <<endl; 
    }
    remove=true;
    for(size_t k=0; k<num_class_; ++k){    
      double Gp = nabla_j[k]+1.0;
      double Gn = nabla_j[k]-1.0;
      double PG = 0.0;
      if(old_w_j[k] == 0.0)
      {
        if(Gp < 0)
          PG = -Gp;
        else if(Gn > 0.0)
          PG = Gn;
        else if(Gp > Gmax_old/l && Gn < -Gmax_old/l)
        {
          new_w_j[k] = 0.0;
          // remove = true;
          // return;
          continue;
        }
        else
          PG=0.0;
      }
      else if(old_w_j[k] > 0.0)
        PG = fabs(Gp);
      else
        PG = fabs(Gn);
      
      Gmax_new = max(Gmax_new, PG);

      solve(new_w_j[k],old_w_j[k],nabla_j[k],h_jj[k]);
      remove=false;
    }

    optimize_armijo(new_w_j,old_w_j,nabla_j,h_jj, column, nnz);
    
    // if(fabs(new_w_j -old_w_j)<10e-12)
    //   return;
    
  }


  void optimize_armijo(vector<ValueType> &new_w_j,
                       const vector<ValueType> &old_w_j,
                       const vector<ValueType> &nabla_j,
                       const vector<ValueType> &h_jj,
                       const char*  column,
                       const size_t nnz
                       ){

    double expected_obj_diff = 0.0;
    for(size_t k=0; k< num_class_ ; k++){
      if(fabs(new_w_j[k]-old_w_j[k])<10e-12)
        continue;
      expected_obj_diff += nabla_j[k] * (new_w_j[k] - old_w_j[k])
        + (fabs(new_w_j[k]) - fabs(old_w_j[k]));
    }
    double h_jj_scaler=1.0;
    for(size_t num_try=0; num_try < 100; ++num_try){
    if(_print){
      cout << "expected_obj_diff " << expected_obj_diff << endl;
    }
 //  can we check lower_bounds like binary classification?
      
      double obj_diff = 0.0;
      for(size_t k=0; k< num_class_ ; k++)
        obj_diff += fabs(new_w_j[k]) - fabs(old_w_j[k]);

      for(int offset=0; offset<nnz*SIZE_OF_UNIT; offset+=SIZE_OF_UNIT){
        const RowIndexType i
          = *reinterpret_cast<const RowIndexType*>(&column[offset])-1;
        const ValueType xij
          = *reinterpret_cast<const ValueType*>(&column[offset+sizeof(RowIndexType)]);                        
        double diff_logsumexp_i = diff_logsumexp(expwTx[i], new_w_j, old_w_j, xij );
        obj_diff += C*diff_logsumexp_i; //(new_logsumexp_i - old_logsumexp_i);
        obj_diff -= C*(new_w_j[y[i]] - old_w_j[y[i]]) * xij;
      }

      if(_print){
        cout << "obj_diff " << obj_diff <<endl;
      }
      if(obj_diff < 0.01 * expected_obj_diff)
        break;

      h_jj_scaler *= 2.0;
      expected_obj_diff *= 0.5;
      for(size_t k=0; k< num_class_ ; ++k ){
//        solve(new_w_j[k],old_w_j[k],nabla_j[k],h_jj[k]*h_jj_scaler);
      // encourage more 0 than just shrinking stepsizes than the below 
        new_w_j[k] = old_w_j[k] + 0.5 * (new_w_j[k] - old_w_j[k]);
      }
    }
  }

  void print(const char* col, const size_t nnz){
    cout << "nnz:" << nnz << " " ;
    for(int offset=0; offset<nnz*SIZE_OF_UNIT; offset+=SIZE_OF_UNIT){
      const RowIndexType i
        = *reinterpret_cast<const RowIndexType*>(&col[offset])-1;
      const ValueType xij
        = *reinterpret_cast<const ValueType*>(&col[offset+sizeof(RowIndexType)]);
      cout << i << ":" << xij << " ";
    }
    cout << endl;
  }
    
  };

#endif
