#ifndef _MULTICLASS_H_
#define _MULTICLASS_H_
typedef unsigned long int ColIndexType;
typedef int RowIndexType;
typedef ColIndexType WeightKeyType;
typedef double ValueType;
#define SIZE_OF_UNIT (sizeof(RowIndexType)+sizeof(ValueType))

#endif
